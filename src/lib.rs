//! A Rust binding to the Tango control system.
//!
//! This crate provides a client API for [Tango control system] servers.
//! It uses the preexisting C bindings (included) and wraps the API functions
//! provided by it in a Rustic interface.
//!
//! [Tango control system]: https://tango-controls.org/
//!
//! Example usage:
//!
//! ```
//! use tango_client::{DeviceProxy, CommandData};
//!
//! let mut dev = DeviceProxy::new("sys/tg_test/1")?;
//! let argin = CommandData::from_str("Test");
//! let argout = dev.command_inout("DevString", arg)?;
//! assert_eq!(argout.into_bytes().unwrap(), "Test".as_bytes());
//! ```
//!
//! At present, only the most used subset of Database and Device APIs are
//! wrapped.  Only synchronous calls are implemented.

// For leak checking:
// use std::alloc::System;
// #[global_allocator]
// static ALLOCATOR: System = System;

#[doc(hidden)]
pub use tango_client_sys as c;


macro_rules! tango_call {
    ($call:ident, $res:expr, $($args:expr),+) => {
        {
            let error_stack = unsafe {
                c::$call($($args,)+)
            };
            if !error_stack.is_null() {
                Err(TangoError::from_stack(error_stack))
            } else {
                Ok($res)
            }
        }
    }
}


pub mod types;
pub mod error;
pub mod proxy;
pub mod dbase;
#[cfg(feature = "serde")]
pub mod serde;

pub use proxy::DeviceProxy;
pub use dbase::DatabaseProxy;
pub use error::{TangoError, TangoResult};
pub use types::{
    TangoDataType,
    TangoDevState,
    DispLevel,
    DevSource,
    CommandInfo,
    CommandData,
    DevEncoded,
    AttributeInfo,
    AttrQuality,
    AttrWriteType,
    AttrDataFormat,
    AttributeData,
    AttrValue,
    DbDatum,
    PropertyValue,
    ErrSeverity,
};
