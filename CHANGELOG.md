# Changelog

## 0.4.2 - Mar 2023

- Fix compatibility with Tango 9.4.

## 0.4.1 - Nov 2021

- Document the crate, make it build on docs.rs.
- Add more valid conversions in the `into_*` methods.

## 0.4.0 - Nov 2021

- Add optional support for `serde` serializing and deserializing of
  `CommandData`, `AttrValue` and `PropertyValue`.
- Add `convert()` methods to all of those types, in order to convert
  to a different `TangoDataType`.

## 0.3.2 - Oct 2021

- Repository moved to gitlab.com/tango-controls.
- Change license to LGPL v3.0+.

## 0.3.1 - Aug 2021

- Small fix to Display output for Error.

## 0.3.0 - Aug 2021

- Add missing `data_type` member to `AttributeInfo`.

## 0.2.3 - Oct 2019

- Fix the case of missing attribute write values.

## 0.2.2 - Oct 2019

- Implement more formatting traits for data types.

## 0.2.1 - Oct 2019

- Fix Tango data types for ARM and 32-bit systems.

## 0.2.0 - Oct 2019

- Provide Display impls for data types.
- Make conversion methods `into_*()` return `Err(self)` instead of `None`.

## 0.1.1 - Jul 2019

- Update to Rust 2018 edition.

## 0.1.0 - Oct 2018

- First release.
